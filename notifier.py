#!/usr/bin/python
from gi import require_version


class Notifier(object):

    def __init__(self, msg, image='./atom-icon.png',
                 head='Atom version checker'):
        require_version('Notify', '0.7')
        from gi.repository import Notify, GdkPixbuf

        Notify.init("Helo")
        self.head = head
        self.img = GdkPixbuf.Pixbuf.new_from_file(image)
        self.n = Notify.Notification().new(head, msg)
        self.n.set_image_from_pixbuf(self.img)
        self.n.show()

    def update(self, msg):
        self.n.update(self.head, msg)
        self.n.set_image_from_pixbuf(self.img)
        self.n.show()
