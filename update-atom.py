#!/usr/bin/python
from atom import AtomVersion
from notifier import Notifier

atom = AtomVersion()
if atom.is_up_to_date():
    Notifier("Atom v{} is up to date!".format(atom.latest))
else:
    msg = 'Downloading v{}: {}%'
    n = Notifier(msg.format(atom.latest, 0))
    path = atom.download_update(lambda x: n.update(msg.format(atom.latest, x)))
    Notifier('New version has been downloaded to {}'.format(path))
