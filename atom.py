#!/usr/bin/python
import os
import subprocess
import requests
from chunker import Chunker


class AtomVersion(object):

    REPO_URL = 'https://api.github.com/repos/atom/atom/releases/latest'
    FILE_NAME = 'atom.x86_64.rpm'

    def _local_atom_version(self):
        output = subprocess.check_output("atom -v", shell=True)
        return output.split('\n')[0].split(':')[1].strip()

    def _latest_atom_version(self):
        self.version_info = requests.get(self.REPO_URL).json()
        return self.version_info['tag_name'][1:]

    def _get_download_link(self):
        self.file_info = filter(lambda a: a['name'] == self.FILE_NAME,
                                self.version_info['assets'])[0]
        return self.file_info['browser_download_url']

    def __init__(self):
        self.local = self._local_atom_version()
        self.latest = self._latest_atom_version()

    def download_update(self, progress_callback):
        link = self._get_download_link()
        path = "{}/{}".format(os.path.expanduser('~'), self.FILE_NAME)
        with open(path, 'wb') as f:
            resp = requests.get(link, stream=True)
            chunker = Chunker(f, self.file_info['size'], progress_callback)
            for chunk in resp.iter_content(chunk_size=65536):
                chunker.write_chunk(chunk)

        return path

    def is_up_to_date(self):
        return self.latest == self.local
