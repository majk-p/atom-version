#!/usr/bin/python
import os


class Chunker(object):

    def __init__(self, f, file_size, progress_callback):
        self.file = f
        self.size = file_size
        self.callback = progress_callback
        self.old_progress = 0

    def _percentage_progress(self, current):
        return int(current) / float(self.size) * 100

    def current_size(self):
        return os.fstat(self.file.fileno()).st_size

    def write_chunk(self, chunk):
        if not chunk:
            return
        self.file.write(chunk)
        progress = self._percentage_progress(self.current_size())
        if progress - self.old_progress > 1:
            self.old_progress = progress
            self.callback(round(progress, 1))
