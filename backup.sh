#!/bin/bash
BACKUP_DIR=${1:-~/.atom-backup}
ATOM_DIR=${2:-~/.atom}

echo "Backing up atom from ${ATOM_DIR} to ${BACKUP_DIR}"
if [ ! -d "${BACKUP_DIR}" ]; then
  mkdir ${BACKUP_DIR}
fi
apm list --installed --bare > ${BACKUP_DIR}/packages.list
cd ${ATOM_DIR}
cp *json ${BACKUP_DIR}
cp *cson ${BACKUP_DIR}
cp *coffee ${BACKUP_DIR}
cp *less ${BACKUP_DIR}
